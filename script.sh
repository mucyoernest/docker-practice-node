#!/bin/bash

# Check if prerequisites are installed and install them if they are not
check_and_install() {
    if ! command -v $1 &> /dev/null
    then
        echo "$1 not found, installing..."
        sudo apt-get update
        sudo apt-get install -y $1
    else
        echo "$1 is already installed"
    fi
}

check_and_install git
check_and_install nodejs
check_and_install npm

# Clone the Github repository
git clone https://github.com/rwieruch/node-express-server-rest-api.git
cd node-express-server-rest-api

# Remove the Git configuration to avoid issues when pushing to another repository
rm -rf .git

# Install JavaScript dependencies
npm install

# Load the env file to system environment variables
if [ -f .env ]; then
  export $(cat .env | xargs)
else
  echo ".env file not found!"
fi
