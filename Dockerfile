# Use the official Node.js image
FROM node:14

# Set the working directory
WORKDIR /usr/src/app

# Copy the shell script into the container
COPY script.sh .

# Copy the application code
COPY . .

# Make the shell script executable
RUN chmod +x script.sh

# Run the shell script
RUN ./script.sh

# Expose the port the app runs on
EXPOSE 3000

# Command to run the application
CMD ["npm", "start"]
